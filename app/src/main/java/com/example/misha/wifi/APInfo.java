package com.example.misha.wifi;

/**
 * Created by adeymike on 2018-04-22.
 *  Class used to create APInfo object, that will store the relevant MAC address and distance
 *  calculation for each access point scanned.
 */

public class APInfo {
    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    private String bssid;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    private double distance;

    public APInfo (String bssid, double distance) {
        this.bssid = bssid;
        this.distance = distance;
    }
}
