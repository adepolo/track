package com.example.misha.wifi;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.DecimalFormat;

/*
    MainActivity Location Tracking, locates the nearest Access Point to your Android Device's location
    (In this case, device used was a OnePlus3T). Displays AP MAC address and estimated distance away.
    LOCATION MUST BE ENABLED IN ORDER FOR DEVICE TO WORK.
     Button may be used for further functionality later on.
* */
public class MainActivity extends Activity {
    private static final String TAG = "";
    // convert double to 3 decimal places
    private static DecimalFormat df2 = new DecimalFormat(".###");

    WifiManager Wifi;
    BroadcastReceiver receiver;
    List<ScanResult> results;
    Button bt1;
    TextView t1;
    @TargetApi(21)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //GUI
        bt1=(Button)findViewById(R.id.btn1);
        // hide button for later possible functionality
        bt1.setVisibility(View.GONE);
        t1=(TextView)findViewById(R.id.label);
        t1.setMovementMethod(new ScrollingMovementMethod());
        t1.setText("");
        // to enable wifi service
        Wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        registerReceiver(mWifiScanReceiver,
                new IntentFilter(Wifi.SCAN_RESULTS_AVAILABLE_ACTION));
        Wifi.setWifiEnabled(true);
        Wifi.startScan();
        /*bt1.setOnClickListener(new View.OnClickListener() {   public void onClick(View arg0) {
            WifiInfo info = Wifi.getConnectionInfo();
            if (info.getBSSID()==null)
            {
                t1.setText("You are currently not connected to any wireless network.\n");
            }
            else
            {
                t1.append("Current Status: \n");
                t1.append("Network Name : "+info.getSSID().toString()+"\n");

                t1.append("RSSI : "+info.getRssi()+"\n");
                t1.append("IP ADDRESS : "+getIpAddress(info)+"\n");
                // actual MAC address
                t1.append("MAC ADDRESS : "+info.getBSSID().toString()+"\n");
                t1.append("Supplicant State : " + info.getSupplicantState().toString() + "\n");
                t1.append("Frequency Speed : " + info.getFrequency()+"\n");
                t1.append("Distance from me: "+ Math.pow(10,(-30-info.getRssi())/20)+"m");
                t1.append("\n\n");


            }
        }
        });*/

    }

    // create the broadcast receiver, in order to successfully scan for wifi list
    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onReceive(Context c, Intent intent) {
            if (intent.getAction().equals(Wifi.SCAN_RESULTS_AVAILABLE_ACTION)) {
                results= Wifi.getScanResults();
                List<APInfo> list = new ArrayList<>();
                //HashMap map = new HashMap();
                // for each result
                for (ScanResult r: results) {
                    //Calculating the level of the Wifi AP signal
                    //int sigLvl = Wifi.calculateSignalLevel(r.level,10);
                    //if the network name is Sheridan Secure Access and the signal level
                    // is greater than 8 (strong signal), display the Network name, along with its
                    // MAC address, signal level and approx. distance away
                    if(r.SSID.toString().equals("Sheridan Secure Access")  /* && sigLvl > 8 */ ) {
                        //display the SSID and MAC address
                        //t1.append("SSID: " + r.SSID.toString() + "\n");
                        //t1.append("MAC: " + r.BSSID.toString() + "\n");
                        //t1.append("Signal level: " + sigLvl+"\n");
                        //// trial and error lead to the txPower approximation - different for every phone
                        //t1.append("Distance away: " + Math.round(calculateDistance(-44,r.level))+"m\n");
                        list.add(new APInfo(r.BSSID.toString(),calculateDistance(-44,r.level)));
                        //map.put(r.BSSID.toString(),calculateDistance(-44,r.level));
                    }
                }
                // sort the list by the shortest distance calculated
                list.sort(Comparator.comparingDouble(APInfo::getDistance));
                t1.append("Access Point closest to me: " +list.get(0).getBssid()+"\n");
                t1.append("Distance: "+ df2.format(list.get(0).getDistance())+"m\n");
            }
        }
    };

    //calculating the distance, using the transmit Power (RSSI at 1 metre and RSSI of result)
    // Method obtained from:
    // https://stackoverflow.com/questions/20416218/understanding-ibeacon-distancing/20434019#20434019
    protected static double calculateDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            double accuracy = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            return accuracy;
        }
    }

    public void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    /*
        During Runtime - Ensuring the Android Permissions were granted
        For Android 6.0 (API 23) and later
        Method from: http://www.northborder-software.com/runtime_permissions.html
     */


    @Override
    public void onResume()
    {
        super.onResume();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 87);
            }
        }
    }
    /*
    private static String getIpAddress(WifiInfo wifiInfo) {
        String result;
        int ip = wifiInfo.getIpAddress();

        result = String.format("%d.%d.%d.%d", (ip & 0xff), (ip >> 8 & 0xff), (ip >> 16 & 0xff),
                (ip >> 24 & 0xff));

        return result;
    }*/






}
